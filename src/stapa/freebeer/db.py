import logging
import pathlib
import random
import sqlite3


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class DBLock:
    def __init__(self):
        logger.debug('DBLock: write Lock initialisation...')
        try:
            import uwsgi
            logger.info('DBLock: uWSGI mode.')
            self.UWSGI = True
        except ImportError:
            logger.info('DBLock: standalone mode.')
            self.UWSGI = False
            from threading import Semaphore
            self.lock = Semaphore()

    def __enter__(self):
        logger.debug('DBLock: acquiring...')
        if self.UWSGI:
            import uwsgi
            uwsgi.lock()
        else:
            self.lock.acquire()
        logger.debug('DBLock: acquired, DB locked.')
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        if self.UWSGI:
            import uwsgi
            uwsgi.unlock()
        else:
            self.lock.release()
        logger.debug('DBLock: released.')


class BadgeDB:
    DATETIME_COLS = []
    """Open and interacts with the Badge DB."""
    def __init__(self, db_path):
        self.conn = sqlite3.connect(db_path, check_same_thread=False)
        self.cur = self.conn.cursor()
        logging.info(f"BadgeDB at '{db_path}' opened.")

        self.db_lock = DBLock()

        if not self.initialized:
            logger.info("BadgeDB: not initialised. Initialising...")
            self.init_db()

    @property
    def initialized(self):
        self.cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
        tables = [row[0] for row in self.cur.fetchall()]
        if not tables:
            return False
        else:
            return True

    def init_db(self):
        import pkgutil
        sql = pkgutil.get_data(__package__, 'schema.sql').decode('utf8')
        with self.db_lock:
            logger.info("DispatcherDB: running init schema...")
            self.cur.executescript(sql)
            self.conn.commit()

    def set_served(self, badge_id):
        with self.db_lock:
            self.cur.execute(
                """
                INSERT INTO served (ts, badge_id)
                VALUES(strftime('%s','now'), ?);
                """, (badge_id,))
            self.conn.commit()

    def set_abuse(self, badge_id):
        with self.db_lock:
            self.cur.execute(
                """
                INSERT INTO abuse (ts, badge_id)
                VALUES(strftime('%s','now'), ?);
                """, (badge_id,))
            self.conn.commit()

    def get_info(self, badge_id):
        info = {}
        with self.db_lock:
            self.cur.execute(
                """
                SELECT COUNT(ts) FROM served
                WHERE
                    badge_id=?
                    AND strftime('%s','now')-ts < 86400;
                """, (badge_id,))
            info['served_last_24h'] = self.cur.fetchone()[0]
            self.cur.execute(
                """
                SELECT COUNT(ts) FROM abuse
                WHERE
                    badge_id=?
                    AND strftime('%s','now')-ts < 86400;
                """, (badge_id,))
            info['abuse_last_24h'] = self.cur.fetchone()[0]
        return info
