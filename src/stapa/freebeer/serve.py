import argparse

import waitress

from . import backend


def serve():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--host', action="store", help='Listen address',
                        default='127.0.0.1')
    parser.add_argument('-p', '--port', action="store", help='Listen port',
                        default=5000)
    args = parser.parse_args()

    waitress.serve(backend.app,
                   host=args.host,
                   port=args.port,
                   url_scheme='http')


if __name__ == '__main__':
    serve()
