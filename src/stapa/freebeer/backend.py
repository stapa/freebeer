from functools import wraps
from pathlib import Path

import requests
from flask import (Flask, render_template, session, request, redirect, url_for,
                   current_app)
from flask_session import Session
from flask_cors import CORS
import msal

from . import app_config
from . import db

app = Flask(__name__)
CORS(app, supports_credentials=True)
app.config.from_object(app_config)
Session(app)

db = db.BadgeDB('badge_db.sql')


def privilege_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        if not session.get('user'):
            return {'reason': 'Not logged in.'}, 401
        if (not check_whitelist(session['user'].get('preferred_username'))
                and not check_in_bureau()):
            return {'reason': 'You are not allowed to user this service.'}, 403
        return f(*args, **kwargs)
    return decorator


@app.route("/")
def index():
    return redirect(url_for('static', filename='index.html'))


@app.route("/session")
def get_session():
    out = {}
    out['logged_in'] = bool(session.get('user'))
    if out['logged_in']:
        out['username'] = session['user'].get('preferred_username')
        out['in_whitelist'] = check_whitelist(out['username'])
        out['in_bureau'] = check_in_bureau()
        out['authorized'] = out['in_whitelist'] or out['in_bureau']
    else:
        msal_init_login()
        out['auth_url'] = session["flow"]["auth_uri"]
    return out


@app.route("/logout")
def logout():
    session.clear()
    return redirect(
        app_config.AUTHORITY + "/oauth2/v2.0/logout" +
        "?post_logout_redirect_uri=" + url_for("index", _external=True))


@app.route("/badge/<badge_id>", methods=["GET"])
@privilege_required
def get_badge(badge_id):
    result = db.get_info(badge_id)
    if result['served_last_24h'] >= 1:
        db.set_abuse(badge_id)
    return result


@app.route("/badge/<badge_id>", methods=["POST"])
@privilege_required
def set_badge(badge_id):
    if request.json.get('served') is True:
        result = db.set_served(badge_id)
    return {'success': True}, 200


def check_in_bureau():
    token = msal_get_token()
    if not token:
        return False
    res = requests.get(
        "https://graph.microsoft.com/v1.0/me/memberOf?$select=id",
        headers={'Authorization': 'Bearer ' + token['access_token']})
    ans = res.json()
    if 'value' not in ans:
        # This means request resulted in error:
        # i.e., memberOf not supported for personal MS accounts.
        return False
    groups_ids = [group['id'] for group in ans['value']]
    return app_config.BUREAU_ID in groups_ids


def check_whitelist(user):
    return user in load_whitelist()


def load_whitelist():
    if not Path('whitelist.txt').is_file():
        return []
    with open('whitelist.txt') as f:
        whitelist = f.readlines()
    whitelist = [line.strip() for line in whitelist]
    return whitelist


def msal_init_login():
    msal_app = msal.ConfidentialClientApplication(
        app_config.CLIENT_ID,
        authority=app_config.AUTHORITY,
        client_credential=app_config.CLIENT_SECRET)

    session["flow"] = msal_app.initiate_auth_code_flow(
        app_config.SCOPE,
        redirect_uri=url_for("msal_token_callback", _external=True))


def msal_get_token():
    token_cache = msal.SerializableTokenCache()
    if session.get('token_cache'):
        token_cache.deserialize(session['token_cache'])

    msal_app = msal.ConfidentialClientApplication(
        app_config.CLIENT_ID,
        authority=app_config.AUTHORITY,
        client_credential=app_config.CLIENT_SECRET,
        token_cache=token_cache)
    accounts = msal_app.get_accounts()
    if accounts:
        result = msal_app.acquire_token_silent(app_config.SCOPE,
                                               account=accounts[0])
        if token_cache.has_state_changed:
            session['token_cache'] = token_cache.serialize()
        return result


@app.route("/getAToken")
def msal_token_callback():
    """Catch MS AD Azure authentication callback"""
    try:
        token_cache = msal.SerializableTokenCache()
        if session.get('token_cache'):
            token_cache.deserialize(session['token_cache'])

        msal_app = msal.ConfidentialClientApplication(
            app_config.CLIENT_ID,
            authority=app_config.AUTHORITY,
            client_credential=app_config.CLIENT_SECRET,
            token_cache=token_cache)

        result = msal_app.acquire_token_by_auth_code_flow(
            session.get("flow", {}),
            request.args)

        if "error" in result:
            return render_template("auth_error.html", result=result)
        session["user"] = result.get("id_token_claims")

        if token_cache.has_state_changed:
            session['token_cache'] = token_cache.serialize()
    except ValueError:  # Usually caused by CSRF
        pass  # Simply ignore them
    return redirect(url_for("index"))
