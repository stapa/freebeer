import xdgappdirs
from configparser import ConfigParser
from pathlib import Path

config_dir = Path(xdgappdirs.user_config_dir("freebeer"))
config_dir.mkdir(parents=True, exist_ok=True)

settings_file = config_dir / "app_settings.ini"

settings = ConfigParser()

if not settings_file.is_file():
    settings['default'] = {
        'CLIENT_ID': "<CLIENT_ID_HERE>",
        'CLIENT_SECRET': "<CLIENT_SECRET_HERE>",
        'AUTHORITY': "https://login.microsoftonline.com/common",
        'REDIRECT_PATH': "/getAToken",
        'SCOPE': "\nUser.ReadBasic.All\nUser.Read",
        'SESSION_TYPE': "filesystem",
        'BUREAU_ID': "68dc9e98-c363-4567-87f4-78cdf4941043"
    }

    with settings_file.open('w') as f:
        settings.write(f)

settings.read(settings_file)

CLIENT_ID = settings['default']['CLIENT_ID']
CLIENT_SECRET = settings['default']['CLIENT_SECRET']

AUTHORITY = settings['default']['AUTHORITY']
REDIRECT_PATH = settings['default']['REDIRECT_PATH']

SCOPE = [line.strip()
         for line in settings['default']['SCOPE'].splitlines()
         if line]
SESSION_TYPE = settings['default']['SESSION_TYPE']
BUREAU_ID = settings['default']['BUREAU_ID']
