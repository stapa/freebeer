<p align="center">
  <a href="https://gitlab.pasteur.fr/stapa/FreeBeer">
    <img src="https://i.ibb.co/28Qyjzq/photo-2022-12-07-02-39-11.jpg" alt="Logo" width="400" />
  </a>

  <h3 align="center">FreeBeer</h3>

  <p align="center">
    A tool to sustainably offer free beers to everyone!
  </p>
</p>